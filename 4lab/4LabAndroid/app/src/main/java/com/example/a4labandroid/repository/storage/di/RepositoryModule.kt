package com.example.a4labandroid.repository.storage.di

import com.example.a4labandroid.repository.storage.TaskRepository
import com.example.a4labandroid.repository.storage.TaskRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Binds
    fun bindRepository(taskRepositoryImpl: TaskRepositoryImpl): TaskRepository
}