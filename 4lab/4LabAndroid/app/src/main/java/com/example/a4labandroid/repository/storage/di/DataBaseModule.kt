package com.example.a4labandroid.repository.storage.di

import android.content.Context
import androidx.room.Room
import com.example.a4labandroid.repository.storage.TaskDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataBaseModule {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, TaskDataBase::class.java, "TakDatabase").build()

    @Provides
    fun provideDao(dataBase: TaskDataBase) = dataBase.getTaskDao()
}