package com.example.a4labandroid.preview_task

data class PreviewTaskState(
    val id: Int? = null,
    val title: String = "",
    val description: String = "",
) {
    val isEdit = id != null
}