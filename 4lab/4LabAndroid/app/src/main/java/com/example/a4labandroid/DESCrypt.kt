package com.example.a4labandroid

import java.util.Base64
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import kotlin.io.encoding.ExperimentalEncodingApi


object DESCrypt {
    const val DESEDE_ENCRYPTION_SCHEME = "DES"
    var keygenerator = KeyGenerator.getInstance(DESEDE_ENCRYPTION_SCHEME)
    var myDesKey = keygenerator.generateKey()

    @OptIn(ExperimentalEncodingApi::class)
    fun encrypt(text: String): String {
        val chipher = Cipher.getInstance(DESEDE_ENCRYPTION_SCHEME)
        val biteArray = text.toByteArray()
        chipher.init(Cipher.ENCRYPT_MODE, myDesKey)
        val chiph = chipher.doFinal(biteArray)
        val res = Base64.getEncoder().encodeToString(chiph)
        return res
    }

    @OptIn(ExperimentalEncodingApi::class)
    fun decrypt(text: String): String {
        val cipher = Cipher.getInstance(DESEDE_ENCRYPTION_SCHEME)
        cipher.init(Cipher.DECRYPT_MODE, myDesKey)
        val res = Base64.getDecoder().decode(text)
        return String(cipher.doFinal(res))
    }
}