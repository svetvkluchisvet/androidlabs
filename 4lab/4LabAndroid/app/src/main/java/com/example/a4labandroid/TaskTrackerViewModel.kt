package com.example.a4labandroid

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a4labandroid.domain.Task
import com.example.a4labandroid.repository.storage.TaskRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TaskTrackerViewModel @Inject constructor(private val taskRepository: TaskRepository) :
    ViewModel() {
    private val _state = MutableStateFlow(TaskTrackerState())
    val state = _state.asStateFlow()

    init {
        viewModelScope.launch {
            taskRepository.getAll().collect(::handleTaskList)
        }
    }

    private fun handleTaskList(list: List<Task>) {
        _state.update { currentState ->
            currentState.copy(taskList = list)
        }
    }

    fun deleteTask(task: Task) {
        viewModelScope.launch {
            taskRepository.delete(task)
        }
    }

    fun changeActive(task: Task, isChanged: Boolean) {
        viewModelScope.launch {
            taskRepository.update(task.copy(completed = isChanged))
        }
    }
}