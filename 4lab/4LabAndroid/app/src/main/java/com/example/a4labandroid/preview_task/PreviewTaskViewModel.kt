package com.example.a4labandroid.preview_task

import android.icu.text.CaseMap.Title
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a4labandroid.DESCrypt
import com.example.a4labandroid.KEY_TASK_ID
import com.example.a4labandroid.domain.Task
import com.example.a4labandroid.repository.storage.TaskRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PreviewTaskViewModel @Inject constructor(
    private val taskRepository: TaskRepository,
    extras: SavedStateHandle,
) :
    ViewModel() {
    private val _state = MutableStateFlow(PreviewTaskState())
    val state = _state.asStateFlow()
    val command = Channel<PreviewTaskCommand>()

    init {
        extras.get<Int>(key = KEY_TASK_ID)?.let { taskId ->
            viewModelScope.launch {
                val task = taskRepository.getTaskById(taskId)
                task?.let {
                    _state.update { currentState ->
                        currentState.copy(
                            id = it.id,
                            title = it.title,
                            description = it.description
                        )
                    }
                    command.send(
                        PreviewTaskCommand.InitField(
                            title = it.title,
                            description = it.description
                        )
                    )
                }
            }

        }
    }

    fun saveTitleText(titleText: String) {
        _state.update { currentState ->
            currentState.copy(title = titleText)
        }
    }

    fun saveDescriptionText(descriptionText: String) {
        _state.update { currentState ->
            currentState.copy(description = descriptionText)
        }
    }

    fun saveTask() {
        val currentState = _state.value
        val task = Task(currentState.id, currentState.title, currentState.description)
        viewModelScope.launch {
            if (currentState.isEdit) {
                taskRepository.update(task)
            } else {
                taskRepository.insert(task)
            }
        }
    }
}