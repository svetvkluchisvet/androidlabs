package com.example.a4labandroid.repository.storage

import com.example.a4labandroid.DESCrypt
import com.example.a4labandroid.domain.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface TaskRepository {

    suspend fun getAll(): Flow<List<Task>>
    suspend fun getTaskById(taskId: Int): Task?
    suspend fun delete(task: Task)
    suspend fun update(task: Task)
    suspend fun insert(task: Task)
}

class TaskRepositoryImpl @Inject constructor(private val taskDao: TaskDao) : TaskRepository {
    override suspend fun getAll(): Flow<List<Task>> = withContext(Dispatchers.IO) {
        taskDao.getAll().map { taskEntityes ->
            taskEntityes.map { taskEntity ->
                Task(
                    id = taskEntity.id ?: 0,
                    title = DESCrypt.decrypt(taskEntity.title),
                    description = DESCrypt.decrypt(taskEntity.description),
                    completed = taskEntity.completed
                )
            }
        }
    }

    override suspend fun getTaskById(taskId: Int): Task? = withContext(Dispatchers.IO) {
        taskDao.getTaskById(taskId)?.let { taskEntity ->
            Task(
                id = taskEntity.id ?: 0,
                title = DESCrypt.decrypt(taskEntity.title),
                description = DESCrypt.decrypt(taskEntity.description),
                completed = taskEntity.completed
            )
        }
    }

    override suspend fun delete(task: Task) = withContext(Dispatchers.IO) {
        val taskEntity = TaskEntity(
            id = task.id,
            title = DESCrypt.encrypt(task.title),
            description = DESCrypt.encrypt(task.description),
            completed = task.completed
        )
        taskDao.delete(taskEntity)
    }

    override suspend fun update(task: Task) = withContext(Dispatchers.IO) {
        val taskEntity = TaskEntity(
            id = task.id,
            title = DESCrypt.encrypt(task.title),
            description = DESCrypt.encrypt(task.description),
            completed = task.completed
        )
        taskDao.update(taskEntity)
    }

    override suspend fun insert(task: Task) = withContext(Dispatchers.IO) {
        val taskEntity = TaskEntity(
            id = task.id,
            title = DESCrypt.encrypt(task.title),
            description = DESCrypt.encrypt(task.description),
            completed = task.completed
        )
        taskDao.saveAllTasks(taskEntity)
    }
}