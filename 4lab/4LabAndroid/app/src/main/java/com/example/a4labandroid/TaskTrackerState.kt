package com.example.a4labandroid

import com.example.a4labandroid.domain.Task

data class TaskTrackerState(val taskList: List<Task> = emptyList())
