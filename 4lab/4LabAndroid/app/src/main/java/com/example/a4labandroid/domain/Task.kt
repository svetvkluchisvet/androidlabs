package com.example.a4labandroid.domain

data class Task(
    val id: Int? = null,
    val title: String = "",
    val description: String = "",
    val completed: Boolean = false,
    //val deadline: Date
) {
    override fun toString(): String {
        return title
    }
}
