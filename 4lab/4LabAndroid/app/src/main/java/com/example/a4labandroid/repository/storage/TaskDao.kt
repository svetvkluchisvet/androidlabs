package com.example.a4labandroid.repository.storage

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.a4labandroid.domain.Task
import kotlinx.coroutines.flow.Flow

@Dao
interface TaskDao {
    @Query("SELECT * FROM Tasks")
    fun getAll(): Flow<List<TaskEntity>>

    @Query("SELECT * FROM Tasks WHERE id =:taskId LIMIT 1") //?
    fun getTaskById(taskId: Int): TaskEntity?

    @Delete
    fun delete(task: TaskEntity)

    @Update
    fun update(task: TaskEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllTasks(vararg task: TaskEntity)
}