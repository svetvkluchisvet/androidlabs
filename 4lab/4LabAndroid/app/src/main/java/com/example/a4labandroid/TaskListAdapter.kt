package com.example.a4labandroid

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a4labandroid.databinding.ItemTaskBinding
import com.example.a4labandroid.domain.Task

class TaskListAdapter(
    val onItemClickListener: (Task) -> Unit,
    val onChangeComplitedListner: (Task, Boolean) -> Unit,
    val onDeleteButtonListner: (Task) -> Unit,
) : ListAdapter<Task, TaskListAdapter.TaskHolder>(TaskComparator()) {

    inner class TaskHolder(private val binding: ItemTaskBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(task: Task) {
            with(binding) {
                tvTitle.text = task.title
                tvDescription.text = task.description
                cbDone.apply {
                    isChecked = task.completed
                    text = if (isChecked) {
                        "Сделано"
                    } else {
                        "Не сделано"
                    }
                    setOnCheckedChangeListener { _, newValue ->
                        onChangeComplitedListner(task, newValue)
                    }
                }
                root.setOnClickListener {
                    onItemClickListener(task)
                }
                ibDelete.setOnClickListener {
                    onDeleteButtonListner(task)
                }
            }
        }
    }

    class TaskComparator : DiffUtil.ItemCallback<Task>() {
        override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
        return TaskHolder(
            binding = ItemTaskBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TaskHolder, position: Int) {
        holder.bind(currentList[position])
    }
}