package com.example.a4labandroid.preview_task

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.a4labandroid.R
import com.example.a4labandroid.databinding.ActivityAddEditTasksBinding
import com.example.a4labandroid.databinding.ActivityMainTaskTrackerBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

@AndroidEntryPoint
class PreviewTaskActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var binding: ActivityAddEditTasksBinding
    private val taskViewModel: PreviewTaskViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddEditTasksBinding.inflate(layoutInflater)
        setContentView(binding.root)
        subscribeState()
        subscribeCommand()
        initListeners()
    }

    private fun subscribeState() {
        launch { taskViewModel.state.collect(::renderScreen) }
    }

    private fun subscribeCommand() {
        launch { taskViewModel.command.receiveAsFlow().collect(::handleCommand) }
    }

    private fun handleCommand(command: PreviewTaskCommand) {
        if (command is PreviewTaskCommand.InitField) {
            with(binding) {
                etTitle.setText(command.title)
                etDescrition.setText(command.description)
            }
        }
    }

    private fun renderScreen(state: PreviewTaskState) {
        val iconLink = if (state.isEdit) {
            R.drawable.baseline_edit_24
        } else {
            R.drawable.baseline_save_alt_24
        }
        binding.fabAdd.setImageResource(iconLink)
    }

    private fun initListeners() {
        with(binding) {
            etTitle.doOnTextChanged { titleText, _, _, _ -> taskViewModel.saveTitleText(titleText.toString()) }
            etDescrition.doOnTextChanged { descriptionText, _, _, _ ->
                taskViewModel.saveDescriptionText(
                    descriptionText.toString()
                )
            }
            fabAdd.setOnClickListener {
                taskViewModel.saveTask()
                finish()
            }

        }
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main
}