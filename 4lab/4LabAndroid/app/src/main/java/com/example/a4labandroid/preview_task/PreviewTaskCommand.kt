package com.example.a4labandroid.preview_task

sealed interface PreviewTaskCommand {
    data class InitField(val title: String, val description: String) : PreviewTaskCommand
}