package com.example.a4labandroid

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.example.a4labandroid.databinding.ActivityMainTaskTrackerBinding
import com.example.a4labandroid.preview_task.PreviewTaskActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

const val KEY_TASK_ID = "KEY_TASK_ID"

@AndroidEntryPoint
class TaskTrackerActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainTaskTrackerBinding
    private val taskTrackerViewModel: TaskTrackerViewModel by viewModels()
    private val taskListAdapter: TaskListAdapter by lazy {
        TaskListAdapter(
            onItemClickListener = { task ->
                goNextScreen(task.id)
            },
            onChangeComplitedListner = { task, done ->
                taskTrackerViewModel.changeActive(task, done)
            },
            onDeleteButtonListner = {
                taskTrackerViewModel.deleteTask(it)
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainTaskTrackerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViews()
        initListners()
        subscribeState()
    }

    private fun initListners() {
        with(binding) {
            fabAdd.setOnClickListener { goNextScreen() }
        }
    }

    private fun goNextScreen(taskId: Int? = null) {
        val intent = Intent(this, PreviewTaskActivity::class.java).apply {
            putExtra(KEY_TASK_ID, taskId)
        }
        startActivity(intent)
    }

    private fun initViews() {
        binding.rvTask.adapter = taskListAdapter
    }

    private fun subscribeState() {
        lifecycleScope.launch {
            taskTrackerViewModel.state.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collect(::renderScreen)
        }
    }

    private fun renderScreen(state: TaskTrackerState) {
        taskListAdapter.submitList(state.taskList)
    }
}