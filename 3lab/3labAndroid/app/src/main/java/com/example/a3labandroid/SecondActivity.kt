package com.example.a3labandroid

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.a3labandroid.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViews()
    }

    private fun initViews() {
        val myText = intent.getStringExtra(KEY_TEXT) ?: "hi, bitch"
        binding.tvInfo.text = myText;
    }
}