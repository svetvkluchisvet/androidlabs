package network

import model.Post
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface APIService {
    @GET("/posts")
    suspend fun getPosts(): List<Post?>?
}