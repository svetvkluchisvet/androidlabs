package com.example.a5labandroid

import model.Post

fun interface RemoteDataStore {
    suspend fun getData(): List<Post>
}