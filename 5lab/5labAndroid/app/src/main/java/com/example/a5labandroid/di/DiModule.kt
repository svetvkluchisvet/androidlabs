package com.example.a5labandroid.di

import com.example.a5labandroid.RemoteDataStore
import com.example.a5labandroid.retrofit.RetrofitRemoteStore
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import network.APIService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
interface DiModule {
    @Binds
    fun bindRetrofit(retrofit: RetrofitRemoteStore): RemoteDataStore
}
