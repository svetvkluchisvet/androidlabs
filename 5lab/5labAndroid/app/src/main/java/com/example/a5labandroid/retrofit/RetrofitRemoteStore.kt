package com.example.a5labandroid.retrofit

import com.example.a5labandroid.RemoteDataStore
import model.Post
import network.APIService
import javax.inject.Inject

class RetrofitRemoteStore @Inject constructor(private val apiService: APIService) :
    RemoteDataStore {
    override suspend fun getData(): List<Post> {
        return apiService.getPosts()?.filterNotNull()?: emptyList()
    }
}