package com.example.a5labandroid

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import java.util.prefs.BackingStoreException

const val BASE_URL = "https://jsonplaceholder.typicode.com"

@HiltAndroidApp
class MainApp() : Application()