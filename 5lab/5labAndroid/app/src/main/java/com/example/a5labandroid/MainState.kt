package com.example.a5labandroid

import model.Post

data class MainState(val listData: List<Post> = emptyList())
