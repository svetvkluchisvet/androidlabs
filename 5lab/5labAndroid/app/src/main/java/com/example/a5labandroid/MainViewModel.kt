package com.example.a5labandroid

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val mainInterceptor: MainInterceptor) :
    ViewModel() {
    val state = MutableStateFlow(MainState())

    init {
        viewModelScope.launch {
            val result = mainInterceptor.getData()
            state.update { it.copy(listData = result) }
        }
    }
}