package com.example.a5labandroid.volley.di

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DiVolleyModule {
    @Provides
    fun provideQueue(@ApplicationContext context: Context): RequestQueue {
        return Volley.newRequestQueue(context)
    }
}
