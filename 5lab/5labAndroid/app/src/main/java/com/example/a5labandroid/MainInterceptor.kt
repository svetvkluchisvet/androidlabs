package com.example.a5labandroid

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import model.Post
import javax.inject.Inject

class MainInterceptor @Inject constructor(private val remoteDataStore: RemoteDataStore) {

    suspend fun getData(): List<Post> = withContext(Dispatchers.IO){
        remoteDataStore.getData()
    }
}