import java.io.File
import java.nio.file.Path

class FileManager : FileWriter<String>, FileReader<String> {
    override fun readingData(fileName: String): Iterable<String> {
        return readFromFile(fileName);
    }

    override fun writingData(fileName: String, data: Iterable<String>) {
        writeToFile(fileName, data)
    }

    private fun readFromFile(fileName: String): Iterable<String> {
        val file = File(fileName)
        return if (file.exists()) {
            file.readLines();
        } else {
            listOf()
        }
    }

    private fun writeToFile(fileName: String, data: Iterable<String>) {
        File(fileName).printWriter().use { out ->
            data.forEach { element ->
                out.println(element)
            }
        }
    }
}

