fun main() {
    val inputFileName = "input.txt"
    val outputFileName = "output.txt"

    val fileManager = FileManager()
    val dataClassifier = DataClassifier()
    val names = fileManager.readingData(inputFileName);
    val sortNames = dataClassifier.sortLines(names);
    fileManager.writingData(outputFileName, sortNames)
}