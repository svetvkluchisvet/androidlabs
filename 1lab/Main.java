class Rectangle {
    double width;
    double height;
    
    double calculateArea() {
        return width * height;
    }
    
    double calculatePerimeter() {
        return 2 * (width + height);
    }
}

class Circle {
    double radius;
    
    double calculateArea() {
        return Math.PI * radius * radius;
    }
    
    double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }
}

class Triangle {
    double a;
    double b;
    double c;

     double calculatePerimeter() {
        return a + b + c;
    }
    
    double calculateArea() {
        return Math.sqrt((calculatePerimeter()/2) * ((calculatePerimeter()/2) - a) * ((calculatePerimeter()/2) - b) * ((calculatePerimeter()/2) - c));
    }
}

public class Main {
    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle();
        rectangle.width = 5;
        rectangle.height = 3;
        
        double rectangleArea = rectangle.calculateArea();
        double rectanglePerimeter = rectangle.calculatePerimeter();
        
        System.out.println("Площадь прямоугольника: " + rectangleArea);
        System.out.println("Периметр прямоугольника: " + rectanglePerimeter);

        Circle circle = new Circle();
        circle.radius = 4;
        
        double circleArea = circle.calculateArea();
        double circlePerimeter = circle.calculatePerimeter();
        
        System.out.println("Площадь круга: " + circleArea);
        System.out.println("Периметр круга: " + circlePerimeter);

        Triangle triangle = new Triangle();
        triangle.a = 4;
        triangle.b = 4;
        triangle.c = 4;

        double trianglePerimeter = triangle.calculatePerimeter();
        double triangleArea = triangle.calculateArea();
        
        System.out.println("Площадь треугольника: " + triangleArea);
        System.out.println("Периметр треугольника: " + trianglePerimeter);
    }
}