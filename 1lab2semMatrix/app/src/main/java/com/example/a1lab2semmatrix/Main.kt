package com.example.a1lab2semmatrix

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

typealias Matrix = List<List<Int>>


/*добавить классы и тесты*/
private val mainScope: CoroutineScope = CoroutineScope(Job() + Dispatchers.Default)
private val resultFlow = MutableSharedFlow<Pair<String, Matrix>>()
private val matrixGenerator = MatrixGenerator()
private val matrixCalculator = MatrixCalculator()

fun main() = runBlocking {
    val heightMatrix: Int = 4
    val widthMatrix: Int = 2
    val commonSizeMatrix: Int = 2
    val job = mainScope.async {
        launch {
            resultFlow.collect { (resultName, matrix) ->
                println(resultName)
                matrix.forEach {
                    println(it)
                }
            }
        }
        val matrix1: Matrix = matrixGenerator.generateMatrix(commonSizeMatrix, widthMatrix)
        val matrix2: Matrix = matrixGenerator.generateMatrix(heightMatrix, commonSizeMatrix)
        val jobSum = launch {
            if (heightMatrix == widthMatrix) {
                val sum: Matrix = matrixCalculator.sumMatrix(matrix1, matrix2)
                resultFlow.emit("Сумма" to sum)
            } else println("Размерность матриц не подходит")
        }

        val jobMultiply = launch {
            val multiply: Matrix = matrixCalculator.multiplyMatrix(matrix1, matrix2)
            resultFlow.emit("Умножение" to multiply)
        }
        val jobTranspose = launch {
            val transpose: Matrix = matrixCalculator.transpose(matrix2)
            resultFlow.emit("Транспонирование" to transpose)
        }

        jobSum.join()
        jobMultiply.join()
        jobTranspose.join()

        println("Матрица1")
        matrix1.forEach {
            println(it)
        }
        println("Матрица2")
        matrix2.forEach {
            println(it)
        }
    }
    job.await()
}
