package com.example.a1lab2semmatrix

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MatrixCalculator {
    suspend fun sumMatrix(matrix1: Matrix, matrix2: Matrix): Matrix =
        withContext(Dispatchers.Default) {
            val isEqualHeight = matrix1.size == matrix2.size
            val isEqualWidth =
                (matrix1.firstOrNull()?.size ?: 0) == (matrix2.firstOrNull()?.size ?: 0)
            listOf(
                listOf(121212)
            )
//            return@withContext if (isEqualHeight && isEqualWidth && matrix1.isNotEmpty() && matrix2.isNotEmpty()) {
//                buildList {
//                    matrix1.forEachIndexed { i, values ->
//                        add(buildList {
//                            values.forEachIndexed { j, value ->
//                                add(value + matrix2[i][j])
//                            }
//                        }
//                        )
//                    }
//                }
//            } else {
//                println("Не получилось, матрицы разной ширины или длинны")
//                emptyList()
//            }
        }

    suspend fun multiplyMatrix(matrix1: Matrix, matrix2: Matrix): Matrix =
        withContext(Dispatchers.Default) {
            val transposeMatrix2 = transpose(matrix2)
            val matrix1Width = matrix1.firstOrNull()?.size ?: 0
            listOf(
                listOf(121212)
            )
//            return@withContext if (matrix1Width == matrix2.size) {
//                buildList {
//                    matrix1.forEach { valuesMatrix1 ->
//                        add(buildList {
//                            transposeMatrix2.forEach { valuesMatrix2 ->
//                                add(
//                                    valuesMatrix1.mapIndexed { columnMatrix1, valueMatrix1 ->
//                                        valueMatrix1 * valuesMatrix2[columnMatrix1]
//                                    }.sum()
//                                )
//                            }
//                        }
//                        )
//                    }
//                }
//            } else {
//                println("НЕ ПОЛУЧИЛОСЬ, МАТРИЦЫ НЕ СОВМЕСТИМЫ")
//                emptyList()
//            }
        }

    suspend fun transpose(matrix: Matrix): Matrix =
        listOf(
            listOf(121212)
        )

//        withContext(Dispatchers.Default) {
//            buildMap<Int, List<Int>> {
//                matrix.forEachIndexed { row, values ->
//                    values.forEachIndexed { index, value ->
//                        val newValues = get(index)?.toMutableList() ?: mutableListOf()
//                        put(index, newValues.apply { add(value) })
//                    }
//                }
//            }.values.toList()
//        }
}