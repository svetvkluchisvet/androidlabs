package com.example.a1lab2semmatrix

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.random.Random

class MatrixGenerator() {

    suspend fun generateMatrix(
        height: Int,
        width: Int,
        minValue: Int = 1,
        maxValue: Int = 10,
    ): Matrix = withContext(Dispatchers.Default) {
        return@withContext if (minValue < maxValue) {
            buildList {
                repeat(height) {
                    add(buildList {
                        repeat(width) {
                            add(Random.nextInt(minValue, maxValue))
                        }
                    })
                }
            }
        } else {
            println("НЕ ПОЛУЧИЛОСЬ, ИЗМЕНИТЕ ЗНАЧЕНИЯ")
            emptyList()
        }
    }
}