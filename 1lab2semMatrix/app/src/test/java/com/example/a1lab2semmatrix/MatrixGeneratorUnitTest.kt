package com.example.a1lab2semmatrix
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class MatrixGeneratorUnitTest {
    val matrixGenerator = MatrixGenerator()

    @Test
    fun `сравнение высоты матриц`() = runBlocking {
        val height = 4
        val width = 2
        val expectedMatrixHeight = 4
        val actualMatrixHeight = matrixGenerator.generateMatrix(height, width).size
        assertEquals(expectedMatrixHeight, actualMatrixHeight)
    }

    @Test
    fun `сравнение высоты матрицы с нулевыми значениями`() = runBlocking {
        val height = 0
        val width = 0
        val expectedMatrixHeight = 0
        val actualMatrixHeight =
            matrixGenerator.generateMatrix(height, width).firstOrNull()?.size ?: 0
        assertEquals(expectedMatrixHeight, actualMatrixHeight)
    }

    @Test
    fun `сравнение ширины матриц`() = runBlocking {
        val height = 4
        val width = 2
        val expectedMatrixWidth = 2
        val actualMatrixWidth =
            matrixGenerator.generateMatrix(height, width).firstOrNull()?.size ?: 0
        assertEquals(expectedMatrixWidth, actualMatrixWidth)
    }

    @Test
    fun `сравнение ширины матриц с нулевыми значениями`() = runBlocking {
        val height = 0
        val width = 0
        val expectedMatrixWidth = 0
        val actualMatrixWidth =
            matrixGenerator.generateMatrix(height, width).firstOrNull()?.size ?: 0
        assertEquals(expectedMatrixWidth, actualMatrixWidth)
    }

    @Test
    fun `проверка диапазона значений, которыми заполняется массив`() = runBlocking {
        val height = 4
        val width = 4
        val expectedMatrixSize = 4
        val actualMatrixSize =
            matrixGenerator.generateMatrix(height, width, 10, 0).size
        assertNotEquals(expectedMatrixSize, actualMatrixSize)
    }
}