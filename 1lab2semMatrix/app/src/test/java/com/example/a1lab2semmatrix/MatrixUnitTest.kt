package com.example.a1lab2semmatrix

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test



class MatrixUnitTest {
    val matrixCalculator = MatrixCalculator()

    @Test
    fun `проверка умножения квадратных матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 2),
            listOf(4, 5),
        )
        val matrix2 = listOf(
            listOf(1, 2),
            listOf(4, 5),
        )
        val matrixResult = listOf(
            listOf(9, 12),
            listOf(24, 33)
        )

        assertEquals(matrixResult, matrixCalculator.multiplyMatrix(matrix1, matrix2))
    }

    @Test
    fun `проверка умножения прямоугольных матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 2, 3),
            listOf(4, 5, 6),
        )
        val matrix2 = listOf(
            listOf(1, 2),
            listOf(4, 5),
            listOf(3, 6),
        )
        val matrixResult = listOf(
            listOf(18, 30),
            listOf(42, 69)
        )

        assertEquals(matrixResult, matrixCalculator.multiplyMatrix(matrix1, matrix2))
    }

    @Test
    fun `проверка умножения матриц с одним эл`() = runBlocking {
        val matrix1 = listOf(
            listOf(1),
        )
        val matrix2 = listOf(
            listOf(1)
        )
        val matrixResult = listOf(
            listOf(1)
        )

        assertEquals(matrixResult, matrixCalculator.multiplyMatrix(matrix1, matrix2))
    }

    @Test
    fun `проверка умножения не совместных матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 2, 3),
            listOf(4, 5, 6),
        )
        val matrix2 = listOf(
            listOf(1, 2),
            listOf(4, 5),
            listOf(3, 6),
            listOf(3, 6),
        )
        val matrixResult = matrixCalculator.multiplyMatrix(matrix1, matrix2).size

        assertEquals(0, matrixResult)
    }

    @Test
    fun `проверка умножения матриц 10 01`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 0),
        )
        val matrix2 = listOf(
            listOf(0),
            listOf(1)
        )
        val matrixResult = listOf(
            listOf(0)
        )

        assertEquals(matrixResult, matrixCalculator.multiplyMatrix(matrix1, matrix2))
    }

    @Test
    fun `проверка транспонирования прямоугольных матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 2, 3),
            listOf(4, 5, 6),
        )

        val expectedTransposeMatrix = listOf(
            listOf(1, 4),
            listOf(2, 5),
            listOf(3, 6),
        )
        val actualTransposeMatrix = matrixCalculator.transpose(matrix1)
        assertEquals(expectedTransposeMatrix, actualTransposeMatrix)
    }


    @Test
    fun `проверка транспонирования квадратных матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 2),
            listOf(4, 5),
        )

        val expectedTransposeMatrix = listOf(
            listOf(1, 4),
            listOf(2, 5),
        )
        val actualTransposeMatrix = matrixCalculator.transpose(matrix1)
        assertEquals(expectedTransposeMatrix, actualTransposeMatrix)
    }

    @Test
    fun `проверка транспонирования матрицы из одного элемента`() = runBlocking {
        val matrix1 = listOf(
            listOf(1)
        )

        val expectedTransposeMatrix = listOf(
            listOf(1)
        )
        val actualTransposeMatrix = matrixCalculator.transpose(matrix1)
        assertEquals(expectedTransposeMatrix, actualTransposeMatrix)
    }

    @Test
    fun `проверка суммирования матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 2),
            listOf(4, 5),
        )
        val matrix2 = listOf(
            listOf(1, 2),
            listOf(4, 5),
        )
        val expectedSumMatrix = listOf(
            listOf(2, 4),
            listOf(8, 10),
        )
        val actualSumMatrix = matrixCalculator.sumMatrix(matrix1, matrix2)
        assertEquals(expectedSumMatrix, actualSumMatrix)
    }

    @Test
    fun `проверка суммирования разноразмерных матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1, 2, 3),
            listOf(4, 5, 6),
        )
        val matrix2 = listOf(
            listOf(1, 2),
            listOf(4, 5),
        )
        val actualSumMatrix = matrixCalculator.sumMatrix(matrix1, matrix2).size
        assertEquals(0, actualSumMatrix)
    }

    @Test
    fun `проверка суммирования единичных матриц`() = runBlocking {
        val matrix1 = listOf(
            listOf(1),
        )
        val matrix2 = listOf(
            listOf(1),
        )
        val expectedSumMatrix = listOf(
            listOf(2)
        )
        val actualSumMatrix = matrixCalculator.sumMatrix(matrix1, matrix2)
        assertEquals(expectedSumMatrix, actualSumMatrix)
    }
}